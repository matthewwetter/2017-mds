<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
  <script src="https://use.typekit.net/crr3swk.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>
  <script src="https://use.fontawesome.com/ccc739af24.js"></script>
</head>
