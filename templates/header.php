<header class="banner">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img class="mds-logo" src="<?= get_template_directory_uri();?>/dist/images/mds-header-logo.jpg" width="150" alt="MDS Associates" /></a>
      </div>
      <div class="col-md-4 contact-phone">
        <?php dynamic_sidebar('header-phone'); ?>
      </div>
      <div class="col-md-4 top-right">
        <div class="row">
          <div class="top-section col-sm-6">
            <?php
            if (has_nav_menu('top_navigation')) :
              wp_nav_menu(['theme_location' => 'top_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
          </div>
          <div class="col-sm-6 col-md-12">
            <?php
            if (has_nav_menu('cart_navigation')) :
              wp_nav_menu(['theme_location' => 'cart_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
            <div class="search-form-container"><?php get_search_form(); ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="top-secondary-menu">
    <div class="container">
      <div class="secondary-menu hidden-xs"><?php dynamic_sidebar('footer-4'); ?></div>
    </div>
  </div>
  <?php if ( is_active_sidebar('special-message') ) { ?>
  <div class="special-offer">
    <div class="container">
      <?php dynamic_sidebar('special-message'); ?>
    </div>
  </div>
  <?php } ?>
  <nav class="nav-primary">
    <div class="container">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </div>
  </nav>
</header>
<?php if ( is_front_page() ) : ?>
  <section class="slider">
    <?php dynamic_sidebar('slider'); ?>
  </section>
<?php endif; ?>
<?php if ( ! is_front_page() && ! is_cart() && is_active_sidebar('hero-banner') ) : ?>
  <section class="hero-banner">
    <div class="container"><?php dynamic_sidebar('hero-banner'); ?></div>
  </section>
<?php endif; ?>