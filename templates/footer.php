<footer class="content-info">
  <div class="social-bar">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <?php dynamic_sidebar('footer-social-left'); ?>
        </div>
        <div class="col-md-4 icons">
          <?php dynamic_sidebar('footer-social'); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="lower-footer">
    <div class="container">
      <div class="row">
      	<div class="col-fifth">
      		<p><a href="https://www.mdsassociates.com/" target="_blank"><img src="<?= get_template_directory_uri();?>/dist/images/mds-logo.png" width="175" alt="MDS Associates - Authorized Distributor" /></a></p>
      		<?php dynamic_sidebar('footer-1'); ?>
      	</div>
      	<div class="col-fifth">
      		<?php dynamic_sidebar('footer-2'); ?>
      	</div>
      	<div class="col-fifth">
      		<?php dynamic_sidebar('footer-3'); ?>
      	</div>
      	<div class="col-fifth">
          <?php dynamic_sidebar('footer-4'); ?>
        </div>
        <div class="col-fifth">
          <?php dynamic_sidebar('footer-5'); ?>
        </div>
      </div>
    </div>
  </div>
  <section class="copyright">
    <div class="container">
      Copyright <script>document.write(new Date().getFullYear())</script> - MDS Associates | All Rights Reserved.
    </div>
  </section>
</footer>