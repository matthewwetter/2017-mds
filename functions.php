<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// REMOVE AUTO LINKING IMAGES
function remove_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );
    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'remove_imagelink_setup', 10);


// REMOVE WORDPRESS EMOJI SCRIPTS

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// WooCommerce Login Button

add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );

function add_loginout_link( $items, $args ) {
   if (is_user_logged_in() && $args->theme_location == 'top_navigation') {
       $items .= '<li class="login-btn"><a href="'.esc_url(home_url('/')).'my-account" class="default-btn login">My Account</a></li>';
   }

   elseif (!is_user_logged_in() && $args->theme_location == 'top_navigation') {
       $items .= '<li class="login-btn"><a href="'.esc_url(home_url('/')).'my-account" class="default-btn login">Sign In</a></li>';
   }

   return $items;
}

/**
 * Adds product SKUs above the "Add to Cart" buttons
 * Tutorial: http://www.skyverge.com/blog/add-information-to-woocommerce-shop-page/
**/
function skyverge_shop_display_skus() {

  global $product;
  
  if ( $product->get_sku() ) {
    echo '<div class="product-meta glove-id">Part #: ' . $product->get_sku() . '</div>';
  }
}
add_action( 'woocommerce_after_shop_loop_item_title', 'skyverge_shop_display_skus', 5 );

// Change SKU text
function translate_woocommerce($translation, $text, $domain) {
    if ($domain == 'woocommerce') {
        switch ($text) {
            case 'SKU':
                $translation = 'Part #';
                break;
            case 'SKU:':
                $translation = 'Part #:';
                break;
        }
    }
    return $translation;
}

add_filter('gettext', 'translate_woocommerce', 10, 3);

// Move WooCommerce Meta
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 5 );
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );        // Remove the description tab
    unset( $tabs['reviews'] );      // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab

    return $tabs;

}

function woocommerce_template_product_description() {
wc_get_template( 'single-product/tabs/description.php' );
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );


// Adding Sold By Info

add_action( 'woocommerce_product_options_general_product_data', 'wc_sold_by', 3 );
function wc_sold_by() {
    // Print a custom text field
    woocommerce_wp_text_input( array(
        'id' => '_wc_sold_by_type',
        'label' => 'Sold By Info',
        'placeholder' => 'ex. Sold by pair, box, pack, etc.'
    ) );
}

add_action( 'woocommerce_process_product_meta', 'wc_save_sold_by' );
function wc_save_sold_by( $post_id ) {
    if ( ! empty( $_POST['_wc_sold_by_type'] ) ) {
        update_post_meta( $post_id, '_wc_sold_by_type', esc_attr( $_POST['_wc_sold_by_type'] ) );
    }
}

//Reposition WooCommerce breadcrumb 
function woocommerce_remove_breadcrumb(){
remove_action( 
    'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
}
add_action(
    'woocommerce_before_main_content', 'woocommerce_remove_breadcrumb'
);

function woocommerce_custom_breadcrumb(){
    woocommerce_breadcrumb();
}

add_action( 'woo_custom_breadcrumb', 'woocommerce_custom_breadcrumb' );


// Change Select Options Text

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
    global $product;    
    $product_type = $product->product_type;  
    switch ( $product_type ) {
    case 'variable':
    case 'simple':
            return __( 'View Product', 'woocommerce' );
            break;
    };
}

// Remove States
add_filter( 'woocommerce_states', 'custom_woocommerce_states' );
 
function custom_woocommerce_states( $states ) {
  $states['US'] = array(
    'AL' => 'Alabama',
    'AK' => 'Alaska',
    'AZ' => 'Arizona',
    'AR' => 'Arkansas',
    'CA' => 'California',
    'CO' => 'Colorado',
    'CT' => 'Connecticut',
    'DE' => 'Delaware',
    'FL' => 'Florida',
    'GA' => 'Georgia',
    'ID' => 'Idaho',
    'IL' => 'Illinois',
    'IN' => 'Indiana',
    'IA' => 'Iowa',
    'KS' => 'Kansas',
    'KY' => 'Kentucky',
    'LA' => 'Louisiana',
    'ME' => 'Maine',
    'MD' => 'Maryland',
    'MA' => 'Massachusetts',
    'MI' => 'Michigan',
    'MN' => 'Minnesota',
    'MS' => 'Mississippi',
    'MO' => 'Missouri',
    'MT' => 'Montana',
    'NE' => 'Nebraska',
    'NM' => 'New Mexico',
    'NV' => 'Nevada',
    'NH' => 'New Hampshire',
    'NJ' => 'New Jersey',
    'NY' => 'New York',
    'NC' => 'North Carolina',
    'ND' => 'North Dakota',
    'OH' => 'Ohio',
    'OK' => 'Oklahoma',
    'OR' => 'Oregon',
    'PA' => 'Pennsylvania',
    'RI' => 'Rhode Island',
    'SC' => 'South Carolina',
    'SD' => 'South Dakota',
    'TN' => 'Tennessee',
    'TX' => 'Texas',
    'UT' => 'Utah',
    'VT' => 'Vermont',
    'VA' => 'Virginia',
    'WA' => 'Washington',
    'WV' => 'West Virginia',
    'WI' => 'Wisconsin',
    'WY' => 'Wyoming'
  );
 
  return $states;
}


// Display SKU on related products slider 

add_action( 'wpb_wrps_slider_before_price', 'wpb_wps_designs_show_sku', 5 );
function wpb_wps_designs_show_sku(){
    global $product;
    $sku = $product->get_sku();
    if( $sku ){
      echo '<div class="wpb-wps-sku"><strong>Part #:</strong> '.$sku.'</div>';
    } 
}

// WooCommere limit zips to 5 digits

function custom_override_checkout_fields2( $fields ) {
$fields['shipping_postcode']['maxlength'] = 5;    
return $fields;
}
add_filter( 'woocommerce_shipping_fields' , 'custom_override_checkout_fields2' );

function custom_override_checkout_fields( $fields ) {
    $fields['billing_postcode']['maxlength'] = 5;    
    return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'custom_override_checkout_fields' );

// Change the add to cart button INTO View Product button
add_filter( 'woocommerce_loop_add_to_cart_link', 'add_product_link' );
function add_product_link( $link ) {
global $product;
    echo '<form action="' . esc_url( $product->get_permalink( $product->id ) ) . '" method="get">
            <button type="submit" class="button add_to_cart_button product_type_simple default-btn">' . __('View Product', 'woocommerce') . '</button>
          </form>';
}


// NUMBERED PAGINATION
function numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="woocommerce"><nav class="woocommerce-pagination"><ul class="page-numbers">' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="current"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="current"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="current"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );

    echo '</ul></nav></div>' . "\n";

}

// Pay by Invoice Option for Logged In Users

add_filter( "woocommerce_available_payment_gateways", "rp_filter_gateways", 9999 );

function rp_filter_gateways($args) {
 if(!is_user_logged_in() && isset($args['cheque'])) {
  unset($args['cheque']);
 }
 return $args;
}

// Prevent PO Box at checkout
add_action('woocommerce_after_checkout_validation', 'deny_pobox_postcode');
function deny_pobox_postcode( $posted ) {
 global $woocommerce;
  $address  = ( isset( $posted['shipping_address_1'] ) ) ?     
 $posted['shipping_address_1'] : $posted['billing_address_1'];
 $postcode = ( isset( $posted['shipping_postcode'] ) ) ?  
 $posted['shipping_postcode'] : $posted['billing_postcode'];
 $replace  = array(" ", ".", ",");
 $address  = strtolower( str_replace( $replace, '', $address ) );
 $postcode = strtolower( str_replace( $replace, '', $postcode ) );
 if ( strstr( $address, 'pobox' ) || strstr( $postcode, 'pobox' ) ) {
   $notice = sprintf( __( '%1$sSorry, we are unable to ship to P.O. Boxes.' , 'error' ) , '<strong>' , '</strong>' );
        if ( version_compare( WC_VERSION, '2.3', '<' ) ) {
            $woocommerce->add_error( $notice );
        } else {
            wc_add_notice( $notice, 'error' );
        }
  }
}

// Hide other shipping options when Free is available

function my_hide_shipping_when_free_is_available( $rates ) {
    $free = array();
    foreach ( $rates as $rate_id => $rate ) {
        if ( 'free_shipping' === $rate->method_id ) {
            $free[ $rate_id ] = $rate;
            break;
        }
    }
    return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

