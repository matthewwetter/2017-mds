<?php get_template_part('templates/page', 'header'); ?>

<h1>Search Results</h1>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
<div class="fluid-container">
	<div class="row">
		<div class="search-results-container">
			<?php while (have_posts()) : the_post(); ?>
				<div class="article-container">
			  		<?php the_post_thumbnail(); ?>
			  		<?php get_template_part('templates/content', 'search'); ?>
			  	</div>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<?php numeric_posts_nav(); ?>
