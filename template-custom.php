<?php
/**
 * Template Name: Breakthrough Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php
	if(get_field('hero_banner')) {
		echo '<div class="hero-banner">' . get_field('hero_banner') . '</div>';
	}
  ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
