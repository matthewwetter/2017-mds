<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <div class="website-container">
      <!--[if IE]>
        <div class="alert alert-warning">
          <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
        </div>
      <![endif]-->
      <?php
        do_action('get_header');
        get_template_part('templates/header');
      ?>
      <?php if ( is_active_sidebar('featured-products') ) : ?>
        <section class="featured-products">
          <div class="container">
            <?php dynamic_sidebar('featured-products'); ?>
          </div>
        </section>
      <?php endif; ?>
      <div class="wrap container" role="document">
        <?php do_action('woo_custom_breadcrumb'); ?>
        <div class="content row">
          <?php if (Setup\display_sidebar()) : ?>
            <main class="col-sm-9">
          <?php endif; ?>
          <?php if (! Setup\display_sidebar() ) : ?>
            <main class="main">
          <?php endif; ?>
            <?php include Wrapper\template_path(); ?>
          </main><!-- /.main -->
          <?php if (Setup\display_sidebar()) : ?>
            <aside class="col-sm-3 sidebar-content">
              <?php include Wrapper\sidebar_path(); ?>
              <div class="visible-xs"><?php dynamic_sidebar('shipping-table'); ?><br><br></div>
            </aside><!-- /.sidebar -->
          <?php endif; ?>
        </div><!-- /.content -->
      </div><!-- /.wrap -->
      <?php if ( is_active_sidebar('testimonials') ) : ?>
        <section class="testimonials-container">
          <div class="container">
            <?php dynamic_sidebar('testimonials'); ?>
          </div>
        </section>
      <?php endif; ?>
      <?php if ( is_active_sidebar('news-footer') ) : ?>
        <section class="recent-news">
          <div class="container">
            <?php dynamic_sidebar('news-footer'); ?>
          </div>
        </section>
      <?php endif; ?>
      <?php
        do_action('get_footer');
        get_template_part('templates/footer');
        wp_footer();
      ?>
    </div>
  </body>
</html>
