/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        
          // Configure/customize these variables.
          $(document).ready(function() {
            $(".show-more a").on("click", function(event) {
                event.preventDefault();
                var $this = $(this); 
                var $content = $this.parent().parent().prev("div.descrip-product");
                var linkText = $this.text().toUpperCase();
                
                if(linkText === "SHOW MORE"){
                    linkText = "Show less";
                    $content.addClass('showContent').removeClass('hideContent');
                } else {
                    linkText = "Show more";
                    $content.addClass('hideContent').removeClass('showContent');
                }

                $this.text(linkText);
            });
            return false;
          });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  // Sub menu appears on mobile click
  $("li.menu-item-has-children").click(function(){
      $(this).find("ul.sub-menu").toggleClass("sub-menu-active");
  });

  // Header Search Box Add Class 

  $(document).mouseup(function (e) {
      var container = $(".search-form-container");
      var hidebtn = $(".search-form-container .search-submit");

      if (!container.is(e.target) && container.has(e.target).length === 0) {
          container.css('display','inline-block');
          hidebtn.hide();
      } else {
          container.css('display','block');
          hidebtn.show();
      }
  });

  // Moved description text into lightbox

  (function($) {
    $("a[data-rel^='lightbox-gallery']").click( function() {
      var desc = $(this).find("img").attr('description');
      $(this).find('img').attr('alt', desc);
    });
  })(jQuery);

})(jQuery); // Fully reference jQuery after this point.
